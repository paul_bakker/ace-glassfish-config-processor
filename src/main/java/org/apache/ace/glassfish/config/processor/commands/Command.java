package org.apache.ace.glassfish.config.processor.commands;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Properties;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public abstract class Command implements Serializable {
    protected Properties properties;
    protected String resourceName;

    public Command(String resourceName, Properties properties) {
       this.properties = properties;
       this.resourceName = resourceName;
    }

    public abstract String asString();
    public abstract String rollbackAsString();

    protected void checkProperties() {
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                if(field.get(this) == null) {
                    throw new IllegalArgumentException(field.getName() + " is required");
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public String getResourceName() {
        return resourceName;
    }
}
