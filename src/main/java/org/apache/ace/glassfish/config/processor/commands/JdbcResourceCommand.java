package org.apache.ace.glassfish.config.processor.commands;

import java.util.Properties;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public class JdbcResourceCommand extends Command {

    private final String jndiName;
    private final String connectionPoolId;

    public JdbcResourceCommand(String resource, Properties properties) {
        super(resource, properties);

        jndiName = properties.getProperty("jndiName");
        connectionPoolId = properties.getProperty("connectionPoolId");

        checkProperties();
    }

    @Override
    public String asString() {
        return new StringBuilder("create-jdbc-resource")
                .append(" --connectionpoolid ").append(connectionPoolId).append(" ")
                .append(jndiName).toString();
    }

    @Override
    public String rollbackAsString() {
         return new StringBuilder("delete-jdbc-resource ")
                .append(jndiName).toString();
    }
}
