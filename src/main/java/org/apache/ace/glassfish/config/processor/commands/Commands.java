package org.apache.ace.glassfish.config.processor.commands;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public enum Commands {
    JDBC_CONNECTION_POOL("jdbc-connection-pool", JdbcConnectionPoolCommand.class),
    JDBC_RESOURCE("jdbc-resource", JdbcResourceCommand.class);

    private String type;
    private Class<? extends Command> commandClass;

    private Commands(String type, Class<? extends Command> commandClass) {
        this.type = type;
        this.commandClass = commandClass;
    }

    public String getType() {
        return type;
    }

    public Class<? extends Command> getCommandClass() {
        return commandClass;
    }

    public boolean supports(String type) {
        return this.type.equals(type);
    }
}
