package org.apache.ace.glassfish.config.processor.commands;

import java.util.Properties;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public class JdbcConnectionPoolCommand extends Command {

    private final String datasourceclassname;
    private final String restype;
    private final String username;
    private final String password;
    private final String url;
    private final String datasourceName;

    public JdbcConnectionPoolCommand(String resource, Properties properties) {
        super(resource,properties);

        datasourceclassname = properties.getProperty("datasourceclassname");
        restype = properties.getProperty("restype");
        username = properties.getProperty("username");
        password = properties.getProperty("password");
        if(properties.getProperty("url") == null) {
            throw new IllegalArgumentException("'url' is required");
        }
        url = properties.getProperty("url").replaceAll("\\:", "\\\\:");
        datasourceName = properties.getProperty("datasourceName");

        checkProperties();
    }

    @Override
    public String asString() {

        StringBuilder props = new StringBuilder()
                .append("user=").append(username).append(":")
                .append("password=").append(password).append(":")
                .append("url=").append(url).append(" ")
                .append(datasourceName);


        StringBuilder sb = new StringBuilder("create-jdbc-connection-pool")
                .append(" --datasourceclassname ").append(datasourceclassname)
                .append(" --restype ").append(restype)
                .append(" --property ").append(props.toString());

        return sb.toString();
    }

    @Override
    public String rollbackAsString() {
        return new StringBuilder("delete-jdbc-connection-pool ")
                .append("--cascade true ")
                .append(datasourceName)
                .toString();

    }
}
