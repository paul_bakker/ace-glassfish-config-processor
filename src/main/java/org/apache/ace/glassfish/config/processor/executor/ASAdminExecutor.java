package org.apache.ace.glassfish.config.processor.executor;

import org.apache.ace.glassfish.config.processor.commands.Command;
import org.osgi.service.log.LogService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public class ASAdminExecutor implements CommandExecutor {
    private volatile LogService m_logService;

    public void execute(Command command) {
        String cmd = command.asString();
        log(LogService.LOG_INFO, "Executing command: " + cmd);
        runCommand(cmd);
    }

    public void rollback(Command command) {
        String cmd = command.rollbackAsString();
        log(LogService.LOG_INFO, "Executing rollback command: " + cmd);
        runCommand(cmd);
    }

    private void runCommand(String cmd) {
        try {
            String glassfish_home = System.getenv("GLASSFISH_HOME");

            String tmp = glassfish_home + "/bin/asadmin " + cmd;
            log(LogService.LOG_DEBUG, "Executing: " + tmp);

            Process p = Runtime.getRuntime().exec(tmp.split(" "));
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                log(LogService.LOG_INFO, line);
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void log(int level, String message) {
        if (m_logService != null) {
            m_logService.log(level, message);
        }
    }
}
