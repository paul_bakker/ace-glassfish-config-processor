package org.apache.ace.glassfish.config.processor;

import org.apache.ace.glassfish.config.processor.commands.Command;
import org.apache.ace.glassfish.config.processor.commands.Commands;
import org.apache.ace.glassfish.config.processor.executor.ASAdminExecutor;
import org.apache.ace.glassfish.config.processor.executor.CommandExecutor;
import org.osgi.framework.BundleContext;
import org.osgi.service.deploymentadmin.spi.DeploymentSession;
import org.osgi.service.deploymentadmin.spi.ResourceProcessor;
import org.osgi.service.deploymentadmin.spi.ResourceProcessorException;
import org.osgi.service.log.LogService;

import java.io.*;
import java.util.*;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public class GlassfishConfigurationResourceProcessor implements ResourceProcessor {
    public final static String PID = "org.apache.ace.glassfish.configuration.processor";
    private volatile DeploymentSession m_session;
    private volatile CommandExecutor m_executor;
    private volatile BundleContext m_bundleContext;
    private volatile LogService m_logService;

    private List<Command> commandsToRun = new ArrayList<Command>();

    public void begin(DeploymentSession session) {
        m_session = session;
    }

    public void process(String name, InputStream stream) throws ResourceProcessorException {
        log(LogService.LOG_INFO, "Processing resource " + name);
        Properties properties = new Properties();
        try {
            properties.load(stream);
        } catch (IOException e) {
            throw new RuntimeException("Error reading properties: ", e);
        }

        String type = (String) properties.get("type");
        for (Commands command : Commands.values()) {
            if (command.supports(type)) {
                try {
                    Command cmd = command.getCommandClass().getConstructor(String.class, Properties.class).newInstance(name, properties);
                    commandsToRun.add(cmd);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

                return;
            }
        }

        throw new RuntimeException("Resource type: " + type + " is unknown. Supported types are " + getSupportedCommands());
    }


    public void dropped(String resource) throws ResourceProcessorException {
        log(LogService.LOG_INFO, "Dropping resource: " + resource);
        for (Command command : commandsToRun) {
            if(command.getResourceName().equals(resource)) {
                log(LogService.LOG_INFO, "Rollback: " + resource);
                m_executor.rollback(command);
                commandsToRun.remove(command);
                return;
            }
        }

        log(LogService.LOG_WARNING, "Command to rollback for " + resource + " not found");
    }

    public void dropAllResources() throws ResourceProcessorException {
        log(LogService.LOG_INFO, "Dropping all resources. " + commandsToRun.size() + " commands in history");
        System.out.println("Dropping all resources. " + commandsToRun.size() + " commands in history");
        try {
            for (Command command : commandsToRun) {
                m_executor.rollback(command);
            }

            commandsToRun.clear();

            File file = new File("commandhistory.ser");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            ResourceProcessorException resourceProcessorException = new ResourceProcessorException(ResourceProcessorException.CODE_OTHER_ERROR);
            resourceProcessorException.initCause(e);
            throw resourceProcessorException;
        }
    }

    public void prepare() throws ResourceProcessorException {
    }

    public void commit() {
        for (Command command : commandsToRun) {
            m_executor.execute(command);
        }
    }

    public void rollback() {
        for (Command command : commandsToRun) {
            m_executor.rollback(command);
        }
    }

    public void cancel() {
        commandsToRun.clear();
    }

    public void start() {

        File file = m_bundleContext.getDataFile("commandhistory.ser");
        if (file.exists()) {
            log(LogService.LOG_INFO, "Reading serialized command history");

            try {
                FileInputStream fileInputStream = null;
                fileInputStream = new FileInputStream(file);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                commandsToRun = (List<Command>) objectInputStream.readObject();
                log(LogService.LOG_INFO, commandsToRun.size() + " commands loaded from history");
            } catch (IOException e) {
                log(LogService.LOG_ERROR, "Error reading command history: " + e.getMessage());
                throw new RuntimeException(e);
            } catch (ClassNotFoundException e) {
                log(LogService.LOG_ERROR, "Error reading command history: " + e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }

    public void stop() {
        if (commandsToRun.size() > 0) {
            log(LogService.LOG_INFO, "Storing command history");
            ObjectOutputStream objectOutputStream = null;
            try {
                FileOutputStream out = new FileOutputStream(m_bundleContext.getDataFile("commandhistory.ser"));
                objectOutputStream = new ObjectOutputStream(out);
                objectOutputStream.writeObject(commandsToRun);
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                if (objectOutputStream != null) {
                    try {
                        objectOutputStream.close();
                    } catch (IOException e) {
                    }
                }
            }
        }
    }

    private String getSupportedCommands() {
        StringBuilder b = new StringBuilder();

        for (Commands command : Commands.values()) {
            b.append(command.getType()).append(", ");
        }

        return b.toString();
    }
    
    private void log(int level, String message) {
        if(m_logService != null) {
            m_logService.log(level, message);
        }
    }
}
