package org.apache.ace.glassfish.config.processor.executor;

import org.apache.ace.glassfish.config.processor.commands.Command;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public interface CommandExecutor {
    void execute(Command command);
    void rollback(Command command);
}
