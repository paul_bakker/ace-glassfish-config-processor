package org.apache.ace.glassfish.config.processor;

import org.apache.ace.client.repository.helper.bundle.BundleHelper;
import org.apache.ace.glassfish.config.processor.executor.ASAdminExecutor;
import org.apache.ace.glassfish.config.processor.executor.CommandExecutor;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.deploymentadmin.spi.ResourceProcessor;
import org.osgi.service.log.LogService;

import java.util.Properties;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public class Activator extends DependencyActivatorBase {
    @Override
    public void init(BundleContext bundleContext, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, GlassfishConfigurationResourceProcessor.PID);
        props.put(BundleHelper.KEY_RESOURCE_PROCESSOR_PID, GlassfishConfigurationResourceProcessor.PID);

        manager.add(manager.createComponent().setInterface(CommandExecutor.class.getName(), null)
                .setImplementation(ASAdminExecutor.class)
                .add(manager.createServiceDependency().setService(LogService.class).setRequired(false)));

        manager.add(manager.createComponent().setInterface(ResourceProcessor.class.getName(), props)
                .setImplementation(GlassfishConfigurationResourceProcessor.class)
                .add(manager.createServiceDependency().setService(CommandExecutor.class).setRequired(true))
                .add(manager.createServiceDependency().setService(LogService.class).setRequired(false)));
    }

    @Override
    public void destroy(BundleContext bundleContext, DependencyManager dependencyManager) throws Exception {
    }
}
