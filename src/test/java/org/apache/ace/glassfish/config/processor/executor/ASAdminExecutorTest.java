package org.apache.ace.glassfish.config.processor.executor;

import org.junit.Test;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public class ASAdminExecutorTest {
    @Test
    public void testExecute() throws Exception {
        Runtime.getRuntime().exec(("/Users/paul/tmp/ace-glassfish/glassfish3/bin/asadmin create-jdbc-connection-pool --datasourceclassname com.mysql.jdbc.jdbc2.optional.MysqlDataSource --restype javax.sql.DataSource --property user=root:password=password:url=jdbc\\:mysql\\://localhost/lom mysqltest3").split(" "));
    }
}
