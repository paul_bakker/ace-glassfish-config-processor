package org.apache.ace.glassfish.config.processor.commands;

import org.junit.Test;

import java.util.Properties;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public class JdbcResourceCommandTest {
    @Test
    public void testAsString() throws Exception {
        Properties properties = new Properties();
        properties.setProperty("type", "jdbc-resource");
        properties.setProperty("jndiName", "myDS");
        properties.setProperty("connectionPoolId", "mypool");

        String command = new JdbcResourceCommand("myresource.gf", properties).asString();
        assertThat(command, is("create-jdbc-resource --connectionpoolid mypool myDS"));
    }
}
