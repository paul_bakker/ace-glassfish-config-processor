package org.apache.ace.glassfish.config.processor;

import org.apache.ace.glassfish.config.processor.commands.Command;
import org.apache.ace.glassfish.config.processor.commands.Commands;
import org.apache.ace.glassfish.config.processor.executor.CommandExecutor;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.service.deploymentadmin.spi.ResourceProcessorException;

import java.io.*;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Properties;

import static junit.framework.Assert.assertTrue;
import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public class GlassfishConfigurationResourceProcessorTest {
    private GlassfishConfigurationResourceProcessor processor;

    @Before
    public void before() {
        processor = new GlassfishConfigurationResourceProcessor();
    }

    @Test
    public void testProcessJdbcConnectionPool() {
        Properties properties = createJdbcConnectionPoolProperties();
        processAndCommit(properties, Commands.JDBC_CONNECTION_POOL);
    }


    @Test
    public void testProcessJdbcResource() {
        Properties properties = createJdbcResourceProperties();

        processAndCommit(properties, Commands.JDBC_RESOURCE);
    }

    @Test
    public void testCommandHistory() throws Exception {
        Properties properties = createJdbcResourceProperties();
        processAndCommit(properties, Commands.JDBC_RESOURCE);

        setBundleContext();

        processor.stop();

        File file = new File("commandhistory.ser");
        assertThat(file.exists(), is(true));

        FileInputStream fileInputStream = new FileInputStream(file);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

        @SuppressWarnings("unchecked")
        List<Command> commands = (List<Command>) objectInputStream.readObject();
        assertNotNull(commands);
        assertThat(commands.size(), is(1));
    }

    private void setBundleContext() {
        BundleContext bundleContext = createMock(BundleContext.class);
        expect(bundleContext.getDataFile("commandhistory.ser")).andReturn(new File("commandhistory.ser"));

        replay(bundleContext);
        setPrivateField(bundleContext);
    }

    @Test
    public void testUnsupportedCommand() throws Exception {
        Properties properties = new Properties();
        properties.setProperty("type", "unsupported");
        try {
            processor.process("wrong.gf", propertiesToInputStream(properties));
            fail("Expected exception because type is not supported");
        } catch (RuntimeException ex) {
            assertTrue(ex.getMessage().contains("Supported types are jdbc-connection-pool"));
        }
    }

    @Test
    public void testRollback() {
        ByteArrayInputStream stream = propertiesToInputStream(createJdbcResourceProperties());

        CommandExecutor executor = createMock(CommandExecutor.class);
        executor.rollback(anyObject(Command.class));

        replay(executor);

        setPrivateField(executor);

        try {
            processor.process("config1.gf", stream);
            processor.rollback();
        } catch (ResourceProcessorException e) {
            throw new RuntimeException(e);
        }

        verify(executor);
    }

    @Test
    public void testCancel() {
        ByteArrayInputStream stream = propertiesToInputStream(createJdbcResourceProperties());

        CommandExecutor executor = createMock(CommandExecutor.class);

        replay(executor);

        setPrivateField(executor);

        try {
            processor.process("config1.gf", stream);
            processor.cancel();
        } catch (ResourceProcessorException e) {
            throw new RuntimeException(e);
        }

        verify(executor);
    }

    @Test
    public void testDropAllResources() throws Exception {
        Properties properties = createJdbcConnectionPoolProperties();
        processAndCommit(properties, Commands.JDBC_CONNECTION_POOL);
        setBundleContext();

        processor.stop();

        processor = new GlassfishConfigurationResourceProcessor();
        setBundleContext();
        processor.start();

        CommandExecutor executor = createMock(CommandExecutor.class);
        executor.rollback(anyObject(Command.class));

        setPrivateField(executor);

        replay(executor);
        processor.dropAllResources();
        File file = new File("commandhistory.ser");
        assertThat(file.exists(), is(false));

        verify(executor);

    }

    @Test
        public void testDropResource() throws Exception {
            Properties properties = createJdbcConnectionPoolProperties();
            processAndCommit(properties, Commands.JDBC_CONNECTION_POOL);
            setBundleContext();

            processor.stop();

            processor = new GlassfishConfigurationResourceProcessor();
            setBundleContext();
            processor.start();

            CommandExecutor executor = createMock(CommandExecutor.class);
            executor.rollback(anyObject(Command.class));

            setPrivateField(executor);

            replay(executor);
            processor.dropped("config1.gf");
            processor.commit();

            verify(executor);

        }


    private Properties createJdbcConnectionPoolProperties() {
        Properties properties = new Properties();
        properties.setProperty("type", "jdbc-connection-pool");
        properties.setProperty("datasourceName", "myCP");
        properties.setProperty("datasourceclassname", "com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        properties.setProperty("restype", "javax.sql.DataSource");
        properties.setProperty("username", "ec2user");
        properties.setProperty("password", "abc");
        properties.setProperty("url", "jdbc:mysql://localhost/mydb");
        return properties;
    }

    private Properties createJdbcResourceProperties() {
        Properties properties = new Properties();
        properties.setProperty("type", "jdbc-resource");
        properties.setProperty("jndiName", "myDS");
        properties.setProperty("connectionPoolId", "myPool");
        return properties;
    }

    private void processAndCommit(Properties properties, Commands command) {
        ByteArrayInputStream stream = propertiesToInputStream(properties);

        CommandExecutor executor = createMock(CommandExecutor.class);
        executor.execute(anyObject(command.getCommandClass()));

        replay(executor);

        setPrivateField(executor);

        try {
            processor.process("config1.gf", stream);
            processor.commit();
        } catch (ResourceProcessorException e) {
            throw new RuntimeException(e);
        }

        verify(executor);
    }

    private ByteArrayInputStream propertiesToInputStream(Properties properties) {
        StringWriter writer = new StringWriter();
        try {
            properties.store(writer, null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return new ByteArrayInputStream(writer.toString().getBytes());
    }

    private void setPrivateField(Object toSet) {
        for (Field field : processor.getClass().getDeclaredFields()) {
            if (field.getType().isAssignableFrom(toSet.getClass())) {
                field.setAccessible(true);
                try {
                    field.set(processor, toSet);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
                return;
            }
        }

        throw new RuntimeException("Field not found");
    }

}
