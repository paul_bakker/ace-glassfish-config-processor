package org.apache.ace.glassfish.config.processor.commands;

import org.junit.Test;

import java.util.Properties;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public class JdbcConnectionPoolCommandTest {
    @Test
    public void testAsString() throws Exception {
        Properties properties = new Properties();
        properties.setProperty("type", "jdbc-connection-pool");
        properties.setProperty("datasourceName", "myCP");
        properties.setProperty("datasourceclassname", "com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        properties.setProperty("restype", "javax.sql.DataSource");
        properties.setProperty("username", "ec2user");
        properties.setProperty("password", "abc");
        properties.setProperty("url", "jdbc:mysql://localhost/mydb");

        String command = new JdbcConnectionPoolCommand("myresource.gf", properties).asString();

        assertThat(command, is("create-jdbc-connection-pool --datasourceclassname com.mysql.jdbc.jdbc2.optional.MysqlDataSource --restype javax.sql.DataSource --property user=ec2user:password=abc:url=jdbc\\:mysql\\://localhost/mydb myCP"));

    }
}
